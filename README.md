# StreamingSystem
Install Git and clo:
```bash
sudo apt install -y git
git clone https://gitlab.com/dq-meister1/streamingsystem.git
```

## Automatic Installation
Convert the `install.sh` file into an executable and run it:
```bash
chmod +x install.sh
sudo ./install.sh
```

## Manual Installation
1. Install (Redis)[https://redis.io/] on the device:
```bash
sudo apt install redis
```
2. Configer Redis to be reachable in your local area network (by standard redis is only available on the Host machine).
Open the `redis.conf` file:
```bash
sudo nano /etc/redis/redis.conf
```
Find: `bind 127.0.0.1 -::1` in the file and change it to `bind 0.0.0.0`, 
as well as `protected-mode yes` to `protected-mode no`.
3. Resart the Redis service:
```bash
sudo systemctl restart redis
```

### Info
You can check the status of `redis` by typing:
```
systemctl status redis
```
The output shud veryfiy that the service is enabled and active.

## Usage
After the installation the redis server should run constantly.
Redis starts automatically when the device is switched on.

## Roadmap
- Testing a cluster setup
- Set-Up a through write with (RedisGears)[https://github.com/RedisGears/rgsync?tab=readme-ov-file] to the master node after a recording.


## Authors and acknowledgment
This project is part of the DQ-Meister project at Furtwangen University

## License
TODO

## Project status
The project is currently still under active development and may therefore contain numerous bugs.