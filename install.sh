#!/bin/bash

# Ensure the script is run as root
if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi

# Update the package list
echo "Update the Device..."
apt-get update
echo "Update Sucsessful."

# Install Redis
echo "Install Redis..."
apt-get install -y redis
echo "Redis Install Sucsessful."

# Configure Redis
echo "Configureing Redis..."
# 1.  Define the Redis configuration file path
REDIS_CONF="/etc/redis/redis.conf"
# 2.  Backup the original configuration file
cp $REDIS_CONF ${REDIS_CONF}.bak
# 3.  Change bind address from 127.0.0.1 -::1 to 0.0.0.0
sed -i '/^bind 127.0.0.1 -::1/s/^bind 127.0.0.1 -::1/bind 0.0.0.0/' $REDIS_CONF
# 4.  Change protected-mode from yes to no
sed -i '/^protected-mode yes/s/^protected-mode yes/protected-mode no/' $REDIS_CONF
# 5.  Restart Redis to apply the changes
systemctl restart redis-server
echo "Configureing Redis Sucsessful..."

echo "Redis installation and configuration update complete."